"use strict";
exports.__esModule = true;
var item_1 = require("./src/item");
var agedBrie_factory_1 = require("./src/factories/agedBrie/agedBrie-factory");
var backstage_factory_1 = require("./src/factories/backstage/backstage-factory");
var conjured_factory_1 = require("./src/factories/conjured/conjured-factory");
var sulfuras_factory_1 = require("./src/factories/sulfuras/sulfuras-factory");
var normal_factory_1 = require("./src/factories/normal/normal-factory");
var GildedRose = /** @class */ (function () {
    function GildedRose(items) {
        if (items === void 0) { items = []; }
        this.items = items;
    }
    GildedRose.prototype.updateQuality = function (factory) {
        var item = factory.updateQualityItem();
        item.qualityValue(new item_1.Item('itemName', 10, 20));
    };
    return GildedRose;
}());
exports["default"] = GildedRose;
var test = new GildedRose();
test.updateQuality(new agedBrie_factory_1["default"]());
test.updateQuality(new backstage_factory_1["default"]());
test.updateQuality(new conjured_factory_1["default"]());
test.updateQuality(new sulfuras_factory_1["default"]());
test.updateQuality(new normal_factory_1["default"]());
