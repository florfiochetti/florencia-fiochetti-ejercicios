"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const item_1 = require("./src/item");
const agedBrie_factory_1 = __importDefault(require("./src/factories/agedBrie/agedBrie-factory"));
const backstage_factory_1 = __importDefault(require("./src/factories/backstage/backstage-factory"));
const conjured_factory_1 = __importDefault(require("./src/factories/conjured/conjured-factory"));
const sulfuras_factory_1 = __importDefault(require("./src/factories/sulfuras/sulfuras-factory"));
const normal_factory_1 = __importDefault(require("./src/factories/normal/normal-factory"));
class GildedRose {
    constructor(items = []) {
        this.items = items;
    }
    updateQuality(factory) {
        const item = factory.updateQualityItem();
        item.qualityValue(new item_1.Item('itemName', 10, 20));
    }
}
exports.default = GildedRose;
const test = new GildedRose();
test.updateQuality(new agedBrie_factory_1.default());
test.updateQuality(new backstage_factory_1.default());
test.updateQuality(new conjured_factory_1.default());
test.updateQuality(new sulfuras_factory_1.default());
test.updateQuality(new normal_factory_1.default());
//# sourceMappingURL=glided-rose.js.map