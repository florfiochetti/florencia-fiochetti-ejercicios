"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const limitsQuality_1 = require("../../limitsQuality");
class ConjuredQuality {
    constructor() {
        this.qualityValue = (item) => {
            item.name = 'Conjured';
            item.sellIn -= 1;
            item.quality -= 2;
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
}
exports.default = ConjuredQuality;
//# sourceMappingURL=conjured-quality.js.map