"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const conjured_quality_1 = __importDefault(require("./conjured-quality"));
class ConjuredFactory {
    updateQualityItem() {
        return new conjured_quality_1.default();
    }
}
exports.default = ConjuredFactory;
//# sourceMappingURL=conjured-factory.js.map