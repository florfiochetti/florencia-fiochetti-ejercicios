"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sulfuras_quality_1 = __importDefault(require("./sulfuras-quality"));
class SulfurasFactory {
    updateQualityItem() {
        return new sulfuras_quality_1.default();
    }
}
exports.default = SulfurasFactory;
//# sourceMappingURL=sulfuras-factory.js.map