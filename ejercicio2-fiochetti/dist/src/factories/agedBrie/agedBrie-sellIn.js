"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MAXIMUM_QUALITY = 50;
// const MINIMUM_QUALITY = 0
const isLessThanMaximum = quality => quality < MAXIMUM_QUALITY;
// const isOverMinimum = quality => quality > MINIMUM_QUALITY
const increaseQuality = quality => isLessThanMaximum(quality) ? quality + 1 : quality;
// const decreaseQuality = quality => isOverMinimum(quality) ? quality - 1 :  quality 
class AgedBrieSellIn {
    constructor() {
        // isLessThanMaximum = quality => quality < 50
        // // const isOverMinimum = quality => quality > MINIMUM_QUALITY
        // increaseQuality = quality => isLessThanMaximum(quality) ? quality + 1 : quality
        // // const decreaseQuality = quality => isOverMinimum(quality) ? quality - 1 :  quality 
        this.open = (item) => {
            item.quality = increaseQuality(item.quality);
            item.quality = item.sellIn < 0 ? increaseQuality(item.quality) : item.quality;
            item.sellIn -= 1;
            console.log('valor de iteeem', item);
            return item;
        };
    }
}
exports.default = AgedBrieSellIn;
//# sourceMappingURL=agedBrie-sellIn.js.map