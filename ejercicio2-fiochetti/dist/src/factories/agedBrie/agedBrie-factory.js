"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const agedBrie_quality_1 = __importDefault(require("./agedBrie-quality"));
class AgedBriedFactory {
    updateQualityItem() {
        return new agedBrie_quality_1.default();
    }
}
exports.default = AgedBriedFactory;
//# sourceMappingURL=agedBrie-factory.js.map