"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const limitsQuality_1 = require("../../limitsQuality");
class AgedBrieQuality {
    constructor() {
        this.qualityValue = (item) => {
            item.name = 'Aged Brie';
            if (item.sellIn > 0) {
                item.sellIn -= 1;
            }
            item.quality += 1;
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
}
exports.default = AgedBrieQuality;
//# sourceMappingURL=agedBrie-quality.js.map