"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const backstage_quality_1 = __importDefault(require("./backstage-quality"));
class BackstageFactory {
    updateQualityItem() {
        return new backstage_quality_1.default();
    }
}
exports.default = BackstageFactory;
//# sourceMappingURL=backstage-factory.js.map