"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const limitsQuality_1 = require("../../limitsQuality");
class BackstageQuality {
    constructor() {
        this.qualityValue = (item) => {
            item.name = 'Backstage';
            if (item.sellIn < 0) {
                item.quality = 0;
            }
            else if (item.sellIn < 6) {
                item.quality += 3;
            }
            else if (item.sellIn < 11) {
                item.quality += 2;
            }
            else {
                item.quality += 1;
            }
            item.sellIn -= 1;
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
}
exports.default = BackstageQuality;
//# sourceMappingURL=backstage-quality.js.map