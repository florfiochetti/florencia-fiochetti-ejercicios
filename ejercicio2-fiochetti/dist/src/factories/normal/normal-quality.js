"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const limitsQuality_1 = require("../../limitsQuality");
class NormalQuality {
    constructor() {
        this.qualityValue = (item) => {
            item.name = 'Normal';
            item.sellIn -= 1;
            item.quality -= 1;
            if (item.sellIn < 0) {
                item.quality -= 1;
            }
            else {
                item.quality = item.quality;
            }
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
}
exports.default = NormalQuality;
//# sourceMappingURL=normal-quality.js.map