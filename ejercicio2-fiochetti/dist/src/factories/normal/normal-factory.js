"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const normal_quality_1 = __importDefault(require("./normal-quality"));
class NormalFactory {
    updateQualityItem() {
        return new normal_quality_1.default();
    }
}
exports.default = NormalFactory;
//# sourceMappingURL=normal-factory.js.map