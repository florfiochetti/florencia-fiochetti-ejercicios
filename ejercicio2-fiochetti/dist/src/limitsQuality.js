"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.qualityLimits = void 0;
const qualityLimits = (item) => {
    if (item.quality < 0) {
        item.quality = 0;
        return item.quality;
    }
    if (item.quality > 50) {
        item.quality = 50;
        return item.quality;
    }
};
exports.qualityLimits = qualityLimits;
//# sourceMappingURL=limitsQuality.js.map