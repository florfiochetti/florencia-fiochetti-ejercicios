import { Item } from "./src/item"
import IAbstractFactory from "./src/abstract-factory.interface";
import AgedBriedFactory from "./src/factories/agedBrie/agedBrie-factory";
import BackstageFactory from "./src/factories/backstage/backstage-factory";
import ConjuredFactory from "./src/factories/conjured/conjured-factory";
import SulfurasFactory from "./src/factories/sulfuras/sulfuras-factory";
import NormalFactory from "./src/factories/normal/normal-factory";


function updateQuality(factory: IAbstractFactory) {
    const item = factory.updateQualityItem();
    item.qualityValue( new Item('itemName',10, 20))
}

    

updateQuality(new AgedBriedFactory());
updateQuality(new BackstageFactory());
updateQuality(new ConjuredFactory());
updateQuality(new SulfurasFactory());
updateQuality(new NormalFactory());

