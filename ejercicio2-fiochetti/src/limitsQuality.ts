import { Item } from "./item";

export const qualityLimits = (item) : Item  =>  {
    
    if (item.quality < 0) {
        item.quality = 0;
        return item.quality
    }
    if (item.quality > 50) {
        item.quality = 50;
        return item.quality
    }
   
}