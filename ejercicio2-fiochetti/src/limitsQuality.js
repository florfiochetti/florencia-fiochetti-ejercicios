"use strict";
exports.__esModule = true;
exports.qualityLimits = void 0;
var qualityLimits = function (item) {
    if (item.quality < 0) {
        item.quality = 0;
        return item.quality;
    }
    if (item.quality > 50) {
        item.quality = 50;
        return item.quality;
    }
};
exports.qualityLimits = qualityLimits;
