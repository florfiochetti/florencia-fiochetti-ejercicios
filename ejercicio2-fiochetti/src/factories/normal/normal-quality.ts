import IQuality from "../quality.interface";
import { Item } from '../../item';
import { qualityLimits } from "../../limitsQuality";

export default class NormalQuality implements IQuality {


    qualityValue = (item) : Item  =>  {
        item.name = 'Normal';

        item.sellIn -= 1;
        item.quality -= 1;

        if(item.sellIn < 0 ){
            item.quality -= 1
        }else{
            item.quality = item.quality
        }  

        qualityLimits(item); 
        console.log(item);
        return item;
    }

}