"use strict";
exports.__esModule = true;
var limitsQuality_1 = require("../../limitsQuality");
var NormalQuality = /** @class */ (function () {
    function NormalQuality() {
        this.qualityValue = function (item) {
            item.name = 'Normal';
            item.sellIn -= 1;
            item.quality -= 1;
            if (item.sellIn < 0) {
                item.quality -= 1;
            }
            else {
                item.quality = item.quality;
            }
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
    return NormalQuality;
}());
exports["default"] = NormalQuality;
