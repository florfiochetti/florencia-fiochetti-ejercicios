import IAbstractFactory from "../../abstract-factory.interface";
import IQuality from "../quality.interface";
import NormalQuality from "./normal-quality";


export default class NormalFactory implements IAbstractFactory {
    
    updateQualityItem(): IQuality {
        return new NormalQuality();
    }
}
