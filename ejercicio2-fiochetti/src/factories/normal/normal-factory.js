"use strict";
exports.__esModule = true;
var normal_quality_1 = require("./normal-quality");
var NormalFactory = /** @class */ (function () {
    function NormalFactory() {
    }
    NormalFactory.prototype.updateQualityItem = function () {
        return new normal_quality_1["default"]();
    };
    return NormalFactory;
}());
exports["default"] = NormalFactory;
