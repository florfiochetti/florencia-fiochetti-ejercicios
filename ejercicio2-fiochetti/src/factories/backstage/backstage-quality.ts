import IQuality from "../quality.interface";
import { Item } from '../../item';
import { qualityLimits } from "../../limitsQuality";


export default class BackstageQuality implements IQuality {

    qualityValue = (item) : Item  =>  {
        item.name = 'Backstage';

        if(item.sellIn < 0){
            item.quality = 0;
        }else if(item.sellIn < 6){
            item.quality += 3;
        }else if(item.sellIn < 11){
            item.quality += 2;
        }else{ 
            item.quality += 1;
        }

        item.sellIn -= 1;
        
        qualityLimits(item);
        console.log(item);
        return item;
    }

}