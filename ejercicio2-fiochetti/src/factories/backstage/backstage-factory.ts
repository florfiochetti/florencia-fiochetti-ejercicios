import IAbstractFactory from "../../abstract-factory.interface";
import IQuality from "../quality.interface";
import BackstageQuality from "./backstage-quality";


export default class BackstageFactory implements IAbstractFactory {
    
    updateQualityItem(): IQuality {
        return new BackstageQuality();
    }
    
}
