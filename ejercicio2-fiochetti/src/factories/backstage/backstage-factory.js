"use strict";
exports.__esModule = true;
var backstage_quality_1 = require("./backstage-quality");
var BackstageFactory = /** @class */ (function () {
    function BackstageFactory() {
    }
    BackstageFactory.prototype.updateQualityItem = function () {
        return new backstage_quality_1["default"]();
    };
    return BackstageFactory;
}());
exports["default"] = BackstageFactory;
