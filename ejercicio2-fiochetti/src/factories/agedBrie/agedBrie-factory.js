"use strict";
exports.__esModule = true;
var agedBrie_quality_1 = require("./agedBrie-quality");
var AgedBriedFactory = /** @class */ (function () {
    function AgedBriedFactory() {
    }
    AgedBriedFactory.prototype.updateQualityItem = function () {
        return new agedBrie_quality_1["default"]();
    };
    return AgedBriedFactory;
}());
exports["default"] = AgedBriedFactory;
