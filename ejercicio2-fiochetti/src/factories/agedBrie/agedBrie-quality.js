"use strict";
exports.__esModule = true;
var limitsQuality_1 = require("../../limitsQuality");
var AgedBrieQuality = /** @class */ (function () {
    function AgedBrieQuality() {
        this.qualityValue = function (item) {
            item.name = 'Aged Brie';
            if (item.sellIn > 0) {
                item.sellIn -= 1;
            }
            item.quality += 1;
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
    return AgedBrieQuality;
}());
exports["default"] = AgedBrieQuality;
