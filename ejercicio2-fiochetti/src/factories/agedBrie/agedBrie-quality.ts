import IQuality from "../quality.interface";
import { Item } from '../../item';
import { qualityLimits } from "../../limitsQuality";

export default class AgedBrieQuality implements IQuality {

    qualityValue = (item) : Item  =>  {
        item.name = 'Aged Brie';

        if(item.sellIn > 0){
            item.sellIn -= 1;
        }
        item.quality += 1;
        
        qualityLimits(item);
        console.log(item);
        return item;
    }

}