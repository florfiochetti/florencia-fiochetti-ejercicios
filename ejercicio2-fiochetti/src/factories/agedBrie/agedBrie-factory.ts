import IAbstractFactory from "../../abstract-factory.interface";
import IQuality from "../quality.interface";
import AgedBrieQuality from "./agedBrie-quality";


export default class AgedBriedFactory implements IAbstractFactory {
    
    updateQualityItem(): IQuality {
        return new AgedBrieQuality();
    }

}
