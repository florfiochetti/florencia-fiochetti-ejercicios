import { Item } from "../item";

export default interface IQuality {
    qualityValue(item: Item): void;
}