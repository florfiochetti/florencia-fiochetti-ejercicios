"use strict";
exports.__esModule = true;
var conjured_quality_1 = require("./conjured-quality");
var ConjuredFactory = /** @class */ (function () {
    function ConjuredFactory() {
    }
    ConjuredFactory.prototype.updateQualityItem = function () {
        return new conjured_quality_1["default"]();
    };
    return ConjuredFactory;
}());
exports["default"] = ConjuredFactory;
