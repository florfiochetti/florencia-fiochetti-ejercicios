import IQuality from "../quality.interface";
import { Item } from '../../item';
import { qualityLimits } from "../../limitsQuality";


export default class ConjuredQuality implements IQuality {

    qualityValue = (item) : Item  =>  {
        item.name = 'Conjured';

        item.sellIn -= 1;
        item.quality -= 2;
              
        qualityLimits(item);
        console.log(item);
        return item;
    }

}