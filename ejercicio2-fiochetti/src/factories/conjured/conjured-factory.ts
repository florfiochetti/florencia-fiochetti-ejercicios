import IAbstractFactory from "../../abstract-factory.interface";
import IQuality from "../quality.interface";
import ConjuredQuality from "./conjured-quality";


export default class ConjuredFactory implements IAbstractFactory {
    
    updateQualityItem(): IQuality {
        return new ConjuredQuality();
    }
    
}
