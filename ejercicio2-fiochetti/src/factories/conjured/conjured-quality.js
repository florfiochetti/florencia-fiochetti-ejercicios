"use strict";
exports.__esModule = true;
var limitsQuality_1 = require("../../limitsQuality");
var ConjuredQuality = /** @class */ (function () {
    function ConjuredQuality() {
        this.qualityValue = function (item) {
            item.name = 'Conjured';
            item.sellIn -= 1;
            item.quality -= 2;
            (0, limitsQuality_1.qualityLimits)(item);
            console.log(item);
            return item;
        };
    }
    return ConjuredQuality;
}());
exports["default"] = ConjuredQuality;
