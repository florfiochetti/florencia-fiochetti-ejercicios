"use strict";
exports.__esModule = true;
var SulfurasQuality = /** @class */ (function () {
    function SulfurasQuality() {
        this.qualityValue = function (item) {
            item.name = 'Sulfuras';
            item.sellIn = 0;
            item.quality = 80;
            console.log(item);
            return item;
        };
    }
    return SulfurasQuality;
}());
exports["default"] = SulfurasQuality;
