import IAbstractFactory from "../../abstract-factory.interface";
import IQuality from "../quality.interface";
import SulfurasQuality from "./sulfuras-quality";


export default class SulfurasFactory implements IAbstractFactory {
    
    updateQualityItem(): IQuality {
        return new SulfurasQuality();
    }
    
}
