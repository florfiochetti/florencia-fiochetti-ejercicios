import IQuality from "../quality.interface";
import { Item } from '../../item';


export default class SulfurasQuality implements IQuality {

    qualityValue = (item) : Item  =>  {
        item.name = 'Sulfuras';

        item.sellIn = 0;
        item.quality = 80;

        console.log(item);
        return item;
    }

}