"use strict";
exports.__esModule = true;
var sulfuras_quality_1 = require("./sulfuras-quality");
var SulfurasFactory = /** @class */ (function () {
    function SulfurasFactory() {
    }
    SulfurasFactory.prototype.updateQualityItem = function () {
        return new sulfuras_quality_1["default"]();
    };
    return SulfurasFactory;
}());
exports["default"] = SulfurasFactory;
