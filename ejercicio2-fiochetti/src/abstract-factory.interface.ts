import IQuality from "./factories/quality.interface";

export default interface IAbstractFactory {
  updateQualityItem(): IQuality;
}

